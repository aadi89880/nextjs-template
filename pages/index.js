import Head from "next/head";
import Card from "../Compponents/Card";

export default function Home() {
  return (
    <div>
      <Head>
        <title>hello its a title!</title>
      </Head>
      <div className="text-3xl"> Welcome to the Portfolio of mine</div>
      <div className="flex items-center justify-center h-screen flex-wrap">
        <div className="flex-auto">
          <Card />
        </div>
        <div className="flex-auto">
          <Card />
        </div>
        <div className="flex-auto">
          <Card />
        </div>
        <div className="flex-auto">
          <Card />
        </div>
      </div>
    </div>
  );
}
