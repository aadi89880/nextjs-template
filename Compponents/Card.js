import React from "react";

const Card = () => {
  return (
    <>
      <div className="rounded-md bg-gray-400 p-3 m-5">
        <h1 className="text-green-400">hello this is a card</h1>
      </div>
    </>
  );
};

export default Card;
